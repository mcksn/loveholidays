package com.mcksn.openadd.adapter;

import com.mcksn.Fixtures;
import com.mcksn.domain.StreetAddress;
import com.mcksn.exception.OpenAddressApiException;
import com.mcksn.openadd.mapping.Addresses;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

public class StreetAddressesResponseAdapterTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testAdaptResponseToInternalFormat() throws Exception {

        //Given
        Addresses addresses = Fixtures.someAddresses();
        ResponseEntity<Addresses> responseEntity = new ResponseEntity(addresses, OK);

        //When
        List<StreetAddress> streetAddressList = new StreetAddressesResponseAdapter().adapt(responseEntity);

        //Then
        assertThat(streetAddressList, hasSize(2));

        assertThat("sao", addresses.getAddresses().get(0).getSao(), is(equalTo("sao1")));
        assertThat("pao", addresses.getAddresses().get(0).getPao(), is(equalTo("pao1")));
        assertThat("street", addresses.getAddresses().get(0).getStreet().getName().getNamesInEN().get(0), is(equalTo("street1")));

        assertThat("sao", addresses.getAddresses().get(1).getSao(), is(equalTo("sao2")));
        assertThat("pao", addresses.getAddresses().get(1).getPao(), is(equalTo("pao2")));
        assertThat("street", addresses.getAddresses().get(1).getStreet().getName().getNamesInEN().get(0), is(equalTo("street2")));

    }

    @Test
    public void testAdaptResponseThrowsExceptionIfResponseBodyIsNull() throws Exception {

        //Given
        thrown.expect(OpenAddressApiException.class);
        thrown.expectMessage(containsString("Response body was empty when parsing response from OpenAdd api."));

        Addresses addresses = null;
        ResponseEntity<Addresses> responseEntity = new ResponseEntity(addresses, OK);

        //When
        new StreetAddressesResponseAdapter().adapt(responseEntity);

        //Then exception
    }

    @Test
    public void testAdaptResponseThrowsExceptionIfResponseIsNull() throws Exception {

        //Given
        thrown.expect(OpenAddressApiException.class);
        thrown.expectMessage(containsString("Response body was empty when parsing response from OpenAdd api."));

        //When
        new StreetAddressesResponseAdapter().adapt(null);

        //Then exception
    }


    @Test
    public void testAdaptResponseThrowsExceptionIfStatusIsNotOK() throws Exception {

        //Given
        thrown.expect(OpenAddressApiException.class);
        thrown.expectMessage(containsString("Failed to retrieve data from OpenAdd api."));

        Addresses addresses = new Addresses(null);
        ResponseEntity<Addresses> responseEntity = new ResponseEntity(addresses, BAD_REQUEST);

        //When
        new StreetAddressesResponseAdapter().adapt(responseEntity);

        //Then exception
    }
}
