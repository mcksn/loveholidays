package com.mcksn;

import com.mcksn.openadd.mapping.Address;
import com.mcksn.openadd.mapping.Address.Street;
import com.mcksn.openadd.mapping.Address.Street.NamesEN;
import com.mcksn.openadd.mapping.Addresses;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Reusable test fixtures
 */
public class Fixtures {

    public static Addresses someAddresses() {
        List<Address> innerAddresses = new ArrayList<>();
        innerAddresses.add(new Address("sao1", "pao1", someStreet("street1")));
        innerAddresses.add(new Address("sao2", "pao2", someStreet("street2")));
        return new Addresses(innerAddresses);
    }

    public static Street someStreet(String names) {
        return new Street(new NamesEN(asList(names)));
    }

}
