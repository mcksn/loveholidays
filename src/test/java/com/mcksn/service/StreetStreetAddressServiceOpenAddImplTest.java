package com.mcksn.service;

import com.mcksn.Fixtures;
import com.mcksn.exception.OpenAddressApiException;
import com.mcksn.openadd.adapter.StreetAddressesResponseAdapter;
import com.mcksn.openadd.mapping.Addresses;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.OK;

@RunWith(MockitoJUnitRunner.class)
public class StreetStreetAddressServiceOpenAddImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private final RestOperations restClient = mock(RestOperations.class);

    private final String baseUrl = "http://example.com";

    private StreetAddressServiceOpenAddImpl streetAddressServiceOpenAddImpl;

    @Before
    public void setUp() throws Exception {
        streetAddressServiceOpenAddImpl = new StreetAddressServiceOpenAddImpl(new StreetAddressesResponseAdapter(), restClient, baseUrl);
        reset(restClient);
    }

    @Test
    public void testGetAddressesTranslatesRestClientExceptionToInternalException() throws Exception {


        //Given
        expectedException.expect(OpenAddressApiException.class);
        expectedException.expectMessage(containsString("Response body was empty when parsing response from OpenAdd api."));

        String postcode = "GL11LF";
        when(restClient.exchange(anyString(), anyObject(), anyObject(), eq(Addresses.class)))
                .thenThrow(new RestClientException("Failed"));

        //When
        streetAddressServiceOpenAddImpl.getAddresses(postcode);

        //Then exception
    }

    @Test
    public void testGetStreetAddressTranslatesRestClientExceptionToInternalException() throws Exception {

        //Given
        expectedException.expect(OpenAddressApiException.class);
        expectedException.expectMessage(containsString("Response body was empty when parsing response from OpenAdd api."));

        String postcode = "GL11LF";
        when(restClient.exchange(anyString(), anyObject(), anyObject(), eq(Addresses.class)))
                .thenThrow(new HttpMessageNotReadableException("Failed"));

        //When
        streetAddressServiceOpenAddImpl.getAddresses(postcode);

        //Then exception
    }


    @Test
    public void testGetStreetAddress() throws Exception {

        //Given
        String postcode = "GL11LF";

        ArgumentCaptor<String> urlCaptor = new ArgumentCaptor<>();

        Map<String, String> queryParams = new HashMap<>(1);
        queryParams.put("postcode", postcode);

        when(restClient.exchange(urlCaptor.capture(), eq(GET), anyObject(), eq(Addresses.class), eq(queryParams)))
                .thenReturn(new ResponseEntity<>(Fixtures.someAddresses(), OK));

        //WHen
        Optional<com.mcksn.domain.StreetAddress> actual = streetAddressServiceOpenAddImpl.getAddresses(postcode);

        //Then
        assertThat(urlCaptor.getValue(), containsString(baseUrl));

        assertTrue("actual not empty", actual.isPresent());

        assertEquals("sao", "sao1", actual.get().sao());
        assertEquals("pao", "pao1", actual.get().pao());
        assertEquals("street", "street1", actual.get().name());
    }
}
