package com.mcksn.integration;

import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.standalone.JsonFileMappingsSource;
import com.mcksn.Application;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import java.text.MessageFormat;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Integration test from spring rest layer to mock OpenAdd api.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Application.class})
@TestPropertySource(locations = "classpath:test.properties")
@WebAppConfiguration
public class StreetAddressControllerApiIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Rule
    public WireMockRule wm = new WireMockRule(options().port(8080).mappingSource(
            new JsonFileMappingsSource(new SingleRootFileSource("src/test/resources"))));

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void should_returnStreetAddressOfPossibleTwo_given_serviceReturnsTwoAddresses() throws Exception {

        //Given
        String postcode = "W60LG";
        String endpoint = MessageFormat.format("/address/street?postcode={0}", postcode);


        //When
        ResultActions result = mockMvc.perform(get(endpoint)
                .accept(APPLICATION_JSON));
                //.andDo(print());

        //Then
        result.andExpect(jsonPath("name", anyOf(equalTo("HAMMERSMITH GROVE"), equalTo("HAMMERSMITH GROVE 2"))));
        result.andExpect(jsonPath("pao", anyOf(equalTo("5-17"), is(equalTo("5-17 2")))));
        result.andExpect(jsonPath("sao", anyOf(equalTo("THE TRIANGLE"), equalTo(null))));

        result.andExpect(status().isOk());

    }

    @Test
    public void should_return404_given_openAddReturnsNoAddresses() throws Exception {

        //Given
        String postcode = "EMPTY";
        String endpoint = MessageFormat.format("/address/street?postcode={0}", postcode);

        //When
        ResultActions result = mockMvc.perform(get(endpoint)
                .accept(APPLICATION_JSON));
                //.andDo(print());

        //Then
        result.andExpect(status().is4xxClientError());

    }

    @Test
    public void should_returnNotFound_given_badEndpoint() throws Exception {

        //Given
        String badEndpoint = "/dontexist";

        //When
        ResultActions result = mockMvc.perform(get(badEndpoint)
                .accept(APPLICATION_JSON));
                //.andDo(print());

        //Then
        result.andExpect(status().isNotFound());
    }


}
