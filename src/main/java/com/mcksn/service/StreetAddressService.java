package com.mcksn.service;

import com.mcksn.domain.StreetAddress;
import com.mcksn.exception.DatasourceException;

import java.util.Optional;

/**
 * Service to retrieve street address.
 */
public interface StreetAddressService {

    /**
     * @return Empty if a street address for the given postcode is not found.
     */
    Optional<StreetAddress> getAddresses(String postcode) throws DatasourceException;

}
