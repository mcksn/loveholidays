package com.mcksn.service;

import com.mcksn.domain.StreetAddress;
import com.mcksn.exception.OpenAddressApiException;
import com.mcksn.openadd.adapter.StreetAddressesResponseAdapter;
import com.mcksn.openadd.mapping.Addresses;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.text.MessageFormat.format;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * Service to retrieve street address.
 * Uses OpenAddress Search API as a datasource.
 * Uses {@link RestOperations} to make HTTP requests.
 */
@Service
public class StreetAddressServiceOpenAddImpl implements StreetAddressService {

    private final Logger log = getLogger(StreetAddressServiceOpenAddImpl.class);

    public final String openAddPostcodeQueryParam = "postcode";
    public final String openAddAddressPath = "addresses.json";
    private final String openAddressUrl;

    private final StreetAddressesResponseAdapter streetAddressesResponseAdapter;

    private final RestOperations restClient;

    @Autowired
    public StreetAddressServiceOpenAddImpl(StreetAddressesResponseAdapter streetAddressesResponseAdapter, RestOperations restClient,
                                           @Value("${openAddress.api.base.url}") String openAddressUrl) {
        this.streetAddressesResponseAdapter = streetAddressesResponseAdapter;
        this.restClient = restClient;
        this.openAddressUrl = openAddressUrl;
    }

    /**
     * Returns a {@link StreetAddress} adapted any one of many {@link Addresses} matches found of the given postcode from OpenAddress api.
     */
    public Optional<StreetAddress> getAddresses(String postcode) throws OpenAddressApiException {

        log.debug("Requesting addresses for postcode {}", postcode);

        Map<String, String> queryParams = new HashMap<>(1);
        queryParams.put(openAddPostcodeQueryParam, postcode);

        ResponseEntity<Addresses> response = request(openAddUrlWithAddressPathAndPostcode(postcode), Addresses.class, queryParams);

        log.debug("Retrieved addresses for postcode {}", postcode);

        return streetAddressesResponseAdapter.adapt(response).stream().findAny();
    }

    private <T> ResponseEntity<T> request(String url, Class<T> responseType, Map<String, String> queryParams) throws OpenAddressApiException {
        try {
            return restClient.exchange(url, GET, createHttpHeaders(), responseType, queryParams);
        } catch (RestClientException | HttpMessageNotReadableException e) {
            throw new OpenAddressApiException("Failed to retrieve data from OpenAdd api", e);
        }
    }

    private HttpEntity<String> createHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(APPLICATION_JSON));
        return new HttpEntity<>("parameters", headers);
    }

    private String openAddUrlWithAddressPathAndPostcode(String postdode) {
        return format("{0}/{1}?{2}={3}", openAddressUrl, openAddAddressPath, openAddPostcodeQueryParam, postdode);
    }
}