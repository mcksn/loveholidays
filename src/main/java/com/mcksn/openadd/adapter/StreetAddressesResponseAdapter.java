package com.mcksn.openadd.adapter;

import com.mcksn.domain.StreetAddress;
import com.mcksn.exception.OpenAddressApiException;
import com.mcksn.openadd.mapping.Addresses;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * An adapter to parse and transform the OpenAdd response into the internal format
 */
@Component
public class StreetAddressesResponseAdapter implements OpenAddResponseAdapter<Addresses, List<StreetAddress>> {

    /**
     * Extracts the addresses from the {@link ResponseEntity} and creates an ordered list
     * of {@link StreetAddress} objects.
     * <p>
     * Assumes response contains an array at addresses.street.name.en. Uses the first values in this array.
     *
     * @throws OpenAddressApiException if the response or response body is null
     */
    public List<StreetAddress> adapt(ResponseEntity<Addresses> addresses) throws OpenAddressApiException {
        validateResponse(addresses);
        return addresses.getBody().getAddresses().stream()
                .map(add -> new StreetAddress(add.getSao(),add.getPao(), add.getStreet().getName().getNamesInEN().get(0)))
                .collect(toList());
    }
}
