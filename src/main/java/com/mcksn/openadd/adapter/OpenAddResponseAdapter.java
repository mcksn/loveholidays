package com.mcksn.openadd.adapter;

import com.mcksn.exception.OpenAddressApiException;
import com.mcksn.openadd.mapping.OpenAddJsonMapping;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.OK;

/**
 * An interface to transform the OpenAdd response into an internal format.
 * Provides a default implementation to handle validation of the HTTP
 * {@link ResponseEntity}.
 */
public interface OpenAddResponseAdapter<T extends OpenAddJsonMapping, U> {

    U adapt(ResponseEntity<T> responseEntity) throws OpenAddressApiException;

    default void validateResponse(ResponseEntity<T> responseEntity) throws OpenAddressApiException {
        if (responseEntity == null || responseEntity.getBody() == null) {
            throw new OpenAddressApiException("Response body was empty when parsing response from OpenAdd api.");
        }
        if (responseEntity.getStatusCode() != OK) {
            throw new OpenAddressApiException("Failed to retrieve data from OpenAdd api.");
        }
    }
}
