package com.mcksn.openadd.mapping;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements OpenAddJsonMapping {

    private final String sao;
    private final String pao;
    private final Street street;

    @JsonCreator
    public Address(@JsonProperty("sao") String sao, @JsonProperty("pao") String pao, @JsonProperty("street") Street street) {
        this.sao = sao;
        this.pao = pao;
        this.street = street;
    }

    public String getSao() {
        return sao;
    }

    public String getPao() {
        return pao;
    }

    public Street getStreet() {
        return street;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Street implements OpenAddJsonMapping {

        private final NamesEN name;

        @JsonCreator
        public Street(@JsonProperty("name") NamesEN name) {
            this.name = name;
        }

        public NamesEN getName() {
            return name;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class NamesEN implements OpenAddJsonMapping {

            private final List<String> namesInEN;

            @JsonCreator
            public NamesEN(@JsonProperty("en") List<String> namesInEN) {
                this.namesInEN = namesInEN;
            }

            public List<String> getNamesInEN() {
                return namesInEN;
            }

        }
    }

}
