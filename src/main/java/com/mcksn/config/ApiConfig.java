package com.mcksn.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.sun.net.ssl.HostnameVerifier;
import com.sun.net.ssl.internal.ssl.*;
import com.sun.net.ssl.internal.ssl.X509ExtendedTrustManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import javax.security.cert.X509Certificate;
import java.security.cert.CertificateException;

@Configuration
public class ApiConfig {

    /**
     * Client to interact with external Restful services
     *
     * @return a standard rest template and error handler
     */
    @Bean
    RestOperations restTemplate() {
        return new RestTemplate();
    }

    /**
     * Provide support for mapping java 8 types
     * when serializing api responses
     *
     * @return custom object mappings for jackson
     */
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Jdk8Module());
        return mapper;
    }

}
