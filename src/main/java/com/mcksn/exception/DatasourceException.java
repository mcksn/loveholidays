package com.mcksn.exception;

public class DatasourceException extends Exception {
    public DatasourceException(String message) {
        super(message);
    }

    public DatasourceException(String message, Throwable t) {
        super(message, t);
    }
}
