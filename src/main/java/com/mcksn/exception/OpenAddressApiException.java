package com.mcksn.exception;

public class OpenAddressApiException extends DatasourceException {

    public OpenAddressApiException(String message) {
        super(message);
    }

    public OpenAddressApiException(String message, Throwable t) {
        super(message, t);
    }
}
