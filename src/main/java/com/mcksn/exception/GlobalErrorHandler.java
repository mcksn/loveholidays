package com.mcksn.exception;

import com.mcksn.service.StreetAddressServiceOpenAddImpl;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@ControllerAdvice
public class GlobalErrorHandler {

    private final Logger log = getLogger(StreetAddressServiceOpenAddImpl.class);

    @ExceptionHandler
    @ResponseStatus(SERVICE_UNAVAILABLE)
    @ResponseBody
    public String handleException(DatasourceException ex) {
        log.error("Handled exception:", ex);
        return "Api error.";
    }

    @ExceptionHandler
    @ResponseStatus(NOT_FOUND)
    @ResponseBody
    public void handleException(StreetAddressNotFoundException ex) {
        return;
    }
}
