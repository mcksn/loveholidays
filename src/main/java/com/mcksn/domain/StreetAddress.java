package com.mcksn.domain;

import com.mcksn.openadd.mapping.OpenAddJsonMapping;

public class StreetAddress implements OpenAddJsonMapping {

    private final String name;
    private final String sao;
    private final String pao;

    public StreetAddress(String sao, String pao, String name) {
        this.sao = sao;
        this.pao = pao;
        this.name = name;
    }

    public String name() {
        return name;
    }

    public String pao() {
        return pao;
    }

    public String sao() {
        return sao;
    }
}
