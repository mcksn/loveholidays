package com.mcksn.resource;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.mcksn.domain.StreetAddress;
import com.mcksn.exception.DatasourceException;
import com.mcksn.exception.OpenAddressApiException;
import com.mcksn.exception.StreetAddressNotFoundException;
import com.mcksn.service.StreetAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("address/street")
public class StreetAddressResource {

    private final StreetAddressService streetAddressService;

    @Autowired
    public StreetAddressResource(StreetAddressService streetAddressService) {
        this.streetAddressService = streetAddressService;
    }

    /**
     * Provides an available street address for the given postcode.
     */
    @RequestMapping
    public Response get(@RequestParam String postcode) throws DatasourceException, StreetAddressNotFoundException {
        StreetAddress streetAddress = streetAddressService.getAddresses(postcode).orElseThrow(StreetAddressNotFoundException::new);
        return new Response(streetAddress);
    }

    static class Response {

        private String name;
        private String pao;
        private String sao;

        public Response(StreetAddress domain) {
            name = domain.name();
            pao = domain.pao();
            sao = domain.sao();
        }

        @JsonGetter
        public String getName() {
            return name;
        }

        @JsonGetter
        public String getPao() {
            return pao;
        }

        @JsonGetter
        public String getSao() {
            return sao;
        }
    }
}
