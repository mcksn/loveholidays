#LoveHolidays Programming Challenge

Mackson Ogu <mackson.ogu@gmail.com>

## Dependencies

1. Maven 3+
2. Java JDK 8

## Structure

The app is comprised a RESTful api written in Java using Spring. Maven is used to manage dependencies and the build.

The underlying api chosen is https://alpha.openaddressesuk.org/

I decided to use Maven instead of Gradle as I dont have a lot of experience with it and didnt want to exceed the suggested time limit.

## Running

Import the CA certificate issued by https://alpha.openaddressesuk.org/ into the cacerts keystore of your JDK.
This way app can trust the data it receives from the api.

Now, execute

     $ mvn clean install
     $ java -jar target/lh-0.0.1-SNAPSHOT.jar
    
The api is now available at http://localhost:9000
